﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movementtransform : MonoBehaviour {
    private float boost1 = 1;
    private float boost2 = 1;
    public float x = 1;
    public float y = 1;
    public float rotatey = 1;
    private ParticleSystem boosterflame;
    void Start()
    {
        boosterflame = GetComponent<ParticleSystem>();
    }
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            transform.Translate(1.5f * (x * boost1), (y * boost2), 0);
            transform.Rotate(0, 1.5f * (rotatey * boost1), 0);
        }
        else
        {
            transform.Translate((x * boost1), (-y * boost2), 0);
            transform.Rotate(0, (rotatey * boost1), 0);
        }
    }
    void Gainboost1()
    {
        boost1 = 1.5f;
        boosterflame.Play();
        Invoke("TimeOut1", 10);
    }
    void TimeOut1()
    {
        boost1 = 1;
        boosterflame.Stop();
    }
    void Gainboost2()
    {
        boost2 = 1.5f;
        Invoke("TimeOut2", 10);
    }
    void TimeOut2()
    {
        boost2 = 1;
    }

    // toinen ratkaisu.
    /*void Update () {
        if (Input.GetKey(KeyCode.Space))
        {
            transform.Translate(1.5f * x * Time.deltaTime, y * Time.deltaTime, 0);
            transform.Rotate(0, 1.5f * rotatey * Time.deltaTime, 0);
        }
        else
        {
            transform.Translate(x * Time.deltaTime, -y * Time.deltaTime, 0);
            transform.Rotate(0, rotatey * Time.deltaTime, 0);
        }
        }*/
}
