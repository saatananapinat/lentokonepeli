﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerupspawnpoint : MonoBehaviour {
    public int spawntime = 1;
    public GameObject powerup;
    void Respawn()
    {
        Invoke("Spawn", spawntime);
    }
    void Spawn()
    {
        Instantiate(powerup, transform.parent).transform.parent = gameObject.transform;
    }
}
