﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour {
    private Component movementscript;
    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            movementscript = collider.GetComponent<Playerscript>();
            movementscript.SendMessage("Gainboost");
            Destroy(gameObject);
        }
    }
}
