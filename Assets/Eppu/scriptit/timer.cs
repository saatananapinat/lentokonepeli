﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timer : MonoBehaviour {
    private int time = 0;
    private Text text;
    private int highscore = 0;

	void Start () {
        Invoke("tick", 1);
        text = gameObject.GetComponent<Text>();
        if (PlayerPrefs.HasKey("Highscore"))
        {
            highscore = PlayerPrefs.GetInt("Highscore");
        }

    }
    void tick()
    {
        time++;
        text.text = time.ToString();
        Invoke("tick", 1);
        if(time > highscore)
        {
            PlayerPrefs.SetInt("Highscore", time);
        }
    }
}
