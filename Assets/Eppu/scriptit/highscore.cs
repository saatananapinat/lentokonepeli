﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class highscore : MonoBehaviour {
    private Text text;
    private int highsc = 0;

    void Start()
    {
        text = gameObject.GetComponent<Text>();
        if (PlayerPrefs.HasKey("Highscore"))
        {
            highsc = PlayerPrefs.GetInt("Highscore");
        }
        text.text = highsc.ToString();
    }
}
