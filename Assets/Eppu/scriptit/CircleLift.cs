﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleLift : MonoBehaviour {

    public GameObject powerspeed;
    public GameObject enemy;
    public float expiretime = 1;
    public float spawntime = 1;
    public float circlespeed = 1;
    public float liftspeed = 1;
    public bool isplayer = false;
    public float boostpower = 1.5f;
    public float maxspawn = 1;
    public float minspawn = 0;
    public float powerspawn = 20f;
    public float spawndistancemax = 0;
    public float spawndistancemin = 0;
    private float boost = 1;
    private float ymove = 0;
    private static float randomnumber;
    void Start()
    {
        if (isplayer == true)
        {
            Invoke("Spawnenemy", spawntime);
            Invoke("powerup", powerspawn);
        }
        else
        {
            transform.Translate(0,-1000,0);
            transform.Rotate(0, Random.Range(spawndistancemin, spawndistancemax), 0);
            Invoke("Die", expiretime);
        }
    }
    void FixedUpdate()
    {

        if (Input.GetKey(KeyCode.Space))
        {
            if (ymove <= 45)
            {
                ymove = ymove + liftspeed;
            }
            transform.Rotate(0, 1.5f * circlespeed * boost, 0);
            transform.Translate(0, (boost * ymove) / 45, 0);
        }
        else
        {
            if (ymove >= -45)
            {
                ymove = ymove - liftspeed;
            }
            transform.Rotate(0, circlespeed * boost, 0);
            transform.Translate(0, (boost * ymove) / 45, 0);
        }
    }
    void Gainboost1()
    {
        Invoke("TimeOut", 10);
        boost = boostpower;
    }
    void TimeOut()
    {
        boost = 1f;
    }
    void Spawnenemy()
    {
        randomnumber = Random.Range(minspawn, maxspawn);
        Instantiate(enemy, new Vector3(gameObject.transform.position.x, randomnumber + 1000, gameObject.transform.position.z), gameObject.transform.rotation);
        Invoke("Spawnenemy", spawntime);
    }
    void powerup()
    {
        randomnumber = Random.Range(minspawn,maxspawn);
        Instantiate(powerspeed, new Vector3(gameObject.transform.position.x, randomnumber + 1000, gameObject.transform.position.z), gameObject.transform.rotation);
        Invoke("powerup",powerspawn);
    }
    void Die()
    {
        Destroy(gameObject);
    }
}
