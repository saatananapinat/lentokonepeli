﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playerscript : MonoBehaviour {
    private float boost = 1;
    private float rotateammount = 0;
    public float rotatespeed = 1;
    private ParticleSystem boosterflame;
    void Start()
    {
        boosterflame = GetComponent<ParticleSystem>();
    }
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            if (rotateammount <= 45)
            {
                transform.Rotate(0, 0, rotatespeed * boost);
                rotateammount = rotateammount + rotatespeed;
            }
        }
        else
        {
            if (rotateammount >= -45)
            {
                transform.Rotate(0, 0, -rotatespeed * boost);
                rotateammount = rotateammount - rotatespeed;
            }
        }
    }
    void Gainboost()
    {
        Invoke("TimeOut", 10);
        boost = 1.5f;
        boosterflame.Play();
        SendMessageUpwards("Gainboost1");
    }
    void TimeOut()
    {
        boost = 1f;
        boosterflame.Stop();
    }
}
